package fleeting

import (
	"context"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"testing"
	"time"

	"github.com/hashicorp/go-hclog"
	dummy "gitlab.com/jobd/fleeting/fleeting/plugin/fleeting-plugin-dummy"
	"gitlab.com/jobd/fleeting/fleeting/provider"
)

func TestProvisionLimits(t *testing.T) {
	tests := []struct {
		knownSize     int
		knownState    provider.State
		expectedSize  int
		maxSize       int
		provisionSize int
	}{
		{1, provider.StateTimeout, 3, 3, 3},
		{1, provider.StateDeleted, 3, 3, 3},
		{1, provider.StateDeleting, 3, 3, 3},
		{1, provider.StateCreating, 3, 3, 3},
		{1, provider.StateCreating, 3, 3, 10},
		{2, provider.StateCreating, 3, 3, 10},
		{5, provider.StateRunning, 5, 10, 10},
		{5, provider.StateRunning, 5, 10, 10},
		{4, provider.StateRunning, 10, 10, 10},
	}

	for _, tc := range tests {
		name := fmt.Sprintf(
			"%d known (%s), expected %d/%d after provisioning %d",
			tc.knownSize,
			tc.knownState,
			tc.expectedSize,
			tc.maxSize,
			tc.provisionSize,
		)

		t.Run(name, func(t *testing.T) {
			group := &dummy.InstanceGroup{MaxSize: tc.maxSize}

			for i := 0; i < tc.knownSize; i++ {
				group.AddInstance(tc.knownState)
			}

			provisioner, err := Init(
				context.Background(),
				nil,
				group,
				WithUpdateInterval(time.Nanosecond),
				WithUpdateIntervalWhenExpecting(time.Nanosecond),
			)
			if err != nil {
				t.Fatal(err)
			}
			defer provisioner.Shutdown(context.Background())

			provisioner.Request(tc.provisionSize)

			var n int
			for i := 0; i < 5; i++ {
				n = len(provisioner.Instances())
				if n == tc.expectedSize {
					break
				}
				time.Sleep(100 * time.Millisecond)
			}

			if n != tc.expectedSize {
				t.Errorf("expected %d, got %d ", tc.expectedSize, n)
			}
		})
	}
}

func TestInstanceReady(t *testing.T) {
	group := &dummy.InstanceGroup{}
	group.AddInstance(provider.StateRunning)
	group.AddInstance(provider.StateCreating)
	group.AddInstance(provider.StateDeleting)

	provisioner, err := Init(
		context.Background(),
		nil,
		group,
		WithUpdateInterval(time.Nanosecond),
		WithUpdateIntervalWhenExpecting(time.Nanosecond),
	)
	if err != nil {
		t.Fatal(err)
	}
	defer provisioner.Shutdown(context.Background())

	instances := provisioner.Instances()
	if len(instances) != 3 {
		t.Fatal("expected 3 instances")
	}

	for _, inst := range instances {
		func() {
			ctx, cancel := context.WithTimeout(context.Background(), time.Second)
			defer cancel()

			switch inst.State() {
			case provider.StateRunning:
				if !inst.ReadyWait(ctx) {
					t.Error("expected running instance to be ready")
				}

			case provider.StateCreating:
				time.AfterFunc(500*time.Millisecond, func() {
					group.SetInstanceState(inst.ID(), provider.StateRunning)
				})

				if !inst.ReadyWait(ctx) {
					t.Error("expected creating instance to be ready after setting state")
				}

			case provider.StateDeleting:
				if inst.ReadyWait(ctx) {
					t.Error("expected deleting instance to never be ready")
				}
			}
		}()
	}
}

func TestInstancePrune(t *testing.T) {
	tests := map[string]struct {
		before     func(*dummy.InstanceGroup, Instance)
		waitFor    provider.State
		after      func(*dummy.InstanceGroup, Instance)
		finalState provider.State
	}{
		"creating->timeout": {
			before: func(group *dummy.InstanceGroup, inst Instance) {
				group.RemoveInstance(inst.ID())
			},
			waitFor:    provider.StateTimeout,
			finalState: provider.StateTimeout,
		},
		"creating->deleting": {
			before: func(group *dummy.InstanceGroup, inst Instance) {
				inst.Delete()
			},
			waitFor: provider.StateDeleting,
			after: func(group *dummy.InstanceGroup, inst Instance) {
				group.RemoveInstance(inst.ID())
			},
			finalState: provider.StateDeleted,
		},
		"provider set deleted": {
			before: func(group *dummy.InstanceGroup, inst Instance) {
				group.SetInstanceState(inst.ID(), provider.StateDeleted)
			},
			waitFor:    provider.StateDeleted,
			finalState: provider.StateDeleted,
		},
		"provider set timeout": {
			before: func(group *dummy.InstanceGroup, inst Instance) {
				group.SetInstanceState(inst.ID(), provider.StateTimeout)
			},
			waitFor:    provider.StateTimeout,
			finalState: provider.StateTimeout,
		},
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			group := &dummy.InstanceGroup{}

			provisioner, err := Init(
				context.Background(),
				nil,
				group,
				WithUpdateInterval(time.Nanosecond),
				WithUpdateIntervalWhenExpecting(time.Nanosecond),
			)
			if err != nil {
				t.Fatal(err)
			}
			defer provisioner.Shutdown(context.Background())

			provisioner.Request(1)
			waitForProvisionerLen(t, provisioner, 1)
			inst := provisioner.Instances()[0]

			if tc.before != nil {
				tc.before(group, inst)
			}

			ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
			defer cancel()
			for inst.State() != tc.waitFor && ctx.Err() == nil {
				time.Sleep(10 * time.Millisecond)
			}

			if tc.after != nil {
				tc.after(group, inst)
			}

			waitForProvisionerLen(t, provisioner, 0)

			if inst.State() != tc.finalState {
				t.Errorf("expected instance to have state %q", tc.finalState)
			}
		})
	}
}

func TestInstanceCause(t *testing.T) {
	group := &dummy.InstanceGroup{}
	group.AddInstance(provider.StateRunning)

	provisioner, err := Init(
		context.Background(),
		nil,
		group,
		WithUpdateInterval(time.Nanosecond),
		WithUpdateIntervalWhenExpecting(time.Nanosecond),
	)
	if err != nil {
		t.Fatal(err)
	}
	defer provisioner.Shutdown(context.Background())

	provisioner.Request(1)
	group.AddInstance(provider.StateDeleting)

	waitForProvisionerLen(t, provisioner, 3)

	for _, inst := range provisioner.Instances() {
		switch inst.State() {
		case provider.StateRunning:
			if inst.Cause() != CausePreexisted {
				t.Error("expected running instance to be preexisting")
			}
		case provider.StateCreating:
			if inst.Cause() != CauseRequested {
				t.Error("expected creating instance to be expected")
			}
		case provider.StateDeleting:
			if inst.Cause() != CauseUnexpected {
				t.Error("expected deleting instance to be unexpected")
			}
		}
	}
}

func TestSubscription(t *testing.T) {
	group := &dummy.InstanceGroup{}
	group.AddInstance(provider.StateCreating)
	group.AddInstance(provider.StateCreating)
	group.AddInstance(provider.StateCreating)

	var instances []Instance
	provisioner, err := Init(
		context.Background(),
		nil,
		group,
		WithUpdateInterval(time.Nanosecond),
		WithUpdateIntervalWhenExpecting(time.Nanosecond),
		WithSubscriber(func(inst []Instance) {
			instances = inst
		}),
	)
	if err != nil {
		t.Fatal(err)
	}
	defer provisioner.Shutdown(context.Background())

	if len(instances) != 3 {
		t.Errorf("expected 3 instances, got %d", len(instances))
	}
}

func TestShutdown(t *testing.T) {
	group := &dummy.InstanceGroup{ManualDelete: true}

	provisioner, err := Init(
		context.Background(),
		nil,
		group,
		WithUpdateInterval(time.Nanosecond),
		WithUpdateIntervalWhenExpecting(time.Nanosecond),
		WithDeletionRetryInterval(0),
		WithShutdownDeletionInterval(50*time.Millisecond),
		WithShutdownDeletionRetries(10),
	)
	if err != nil {
		t.Fatal(err)
	}
	defer provisioner.Shutdown(context.Background())

	provisioner.Request(1)
	waitForProvisionerLen(t, provisioner, 1)

	inst := provisioner.Instances()[0]
	inst.Delete()

	time.AfterFunc(1*time.Second, func() {
		group.SetInstanceState(inst.ID(), provider.StateDeleting)
	})
	provisioner.Shutdown(context.Background())

	provisioner.Request(1)
	if len(provisioner.request) > 0 {
		t.Errorf("expected 0 request length, got %d", len(provisioner.request))
	}
}

func TestRunPlugin(t *testing.T) {
	dir := t.TempDir()

	binaryName := "fleeting-plugin-dummy"
	if runtime.GOOS == "windows" {
		binaryName += ".exe"
	}
	binaryName = filepath.Join(dir, binaryName)

	target, _ := filepath.Abs("plugin/fleeting-plugin-dummy/cmd/fleeting-plugin-dummy")

	cmd := exec.Command("go", "build", "-o", binaryName, target)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	runner, err := RunPlugin(binaryName, []byte(`{"MaxBuild": 1}`))
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	defer runner.Kill()

	provisioner, err := Init(
		context.Background(),
		nil,
		runner.InstanceGroup(),
		WithUpdateInterval(time.Nanosecond),
		WithUpdateIntervalWhenExpecting(time.Nanosecond),
	)
	if err != nil {
		t.Fatal(err)
	}
	defer provisioner.Shutdown(context.Background())

	provisioner.Request(1)
	waitForProvisionerLen(t, provisioner, 1)
	inst := provisioner.Instances()[0]

	_, err = inst.ConnectInfo(context.Background())
	if err != nil {
		t.Fatal(err)
	}
}

func waitForProvisionerLen(t *testing.T, provisioner *Provisioner, n int) {
	var total int
	for i := 0; i < 10; i++ {
		total = len(provisioner.Instances())
		if total == n {
			break
		}
		time.Sleep(time.Second)
	}

	if n != total {
		t.Errorf("expected %d instances, got %d", n, total)
	}
}

func BenchmarkProvisioner(b *testing.B) {
	b.ReportAllocs()

	subCh := make(chan Instance, 100)
	for n := 0; n < b.N; n++ {
		provisioner, err := Init(
			context.Background(),
			hclog.NewNullLogger(),
			&dummy.InstanceGroup{},
			WithUpdateInterval(time.Nanosecond),
			WithUpdateIntervalWhenExpecting(time.Nanosecond),
			WithShutdownDeletionRetries(0),
			WithSubscriber(func(instances []Instance) {
				for _, inst := range instances {
					subCh <- inst
				}
			}),
		)

		if err != nil {
			panic(err)
		}

		go func() {
			for i := 0; i < 10000; i++ {
				provisioner.Request(1)
			}
		}()

		for i := 0; i < 10000; i++ {
			inst := <-subCh
			inst.Delete()
		}

		provisioner.Shutdown(context.Background())
	}
}
