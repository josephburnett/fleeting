//go:build windows

package main

import (
	"fmt"
	"io"
	"net"
	"os"
	"time"

	"github.com/Microsoft/go-winio"
)

func proxy(w io.Writer, r io.Reader, network, address string, timeout time.Duration) error {
	if network != "npipe" {
		conn, err := net.DialTimeout(network, address, timeout)
		if err != nil {
			return fmt.Errorf("proxy dialing: %w", err)
		}

		return do(w, r, conn, timeout, os.ErrClosed)
	}

	return npipe(w, r, address, timeout)
}

func npipe(w io.Writer, r io.Reader, address string, timeout time.Duration) error {
	conn, err := winio.DialPipe(address, &timeout)
	if err != nil {
		return fmt.Errorf("proxy dialing: %w", err)
	}

	return do(w, r, conn, timeout, winio.ErrFileClosed)
}
