//go:build !windows

package main

import (
	"fmt"
	"io"
	"net"
	"os"
	"time"
)

func proxy(w io.Writer, r io.Reader, network, address string, timeout time.Duration) error {
	conn, err := net.DialTimeout(network, address, timeout)
	if err != nil {
		return fmt.Errorf("proxy dialing: %w", err)
	}

	return do(w, r, conn, timeout, os.ErrClosed)
}
