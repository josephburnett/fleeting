package connector

import (
	"context"
	"fmt"
	"io"
	"net"
	"time"

	"gitlab.com/jobd/fleeting/fleeting/provider"
)

type RunOptions struct {
	Command string
	Stdin   io.Reader
	Stdout  io.Writer
	Stderr  io.Writer
}

type DialOptions struct {
	UseExternalAddr bool
}

type ConnectorOptions struct {
	RunOptions
	UseExternalAddr bool
}

type Client interface {
	Run(opts RunOptions) error
	Dial(n string, addr string) (net.Conn, error)
	Close() error
}

func Dial(ctx context.Context, info provider.ConnectInfo, options DialOptions) (Client, error) {
	switch info.Protocol {
	default:
		client, err := DialSSH(ctx, info, options)
		if err != nil {
			return nil, fmt.Errorf("dial ssh: %w", err)
		}
		return client, nil

	case provider.ProtocolWinRM:
		client, err := DialWinRM(ctx, info, options)
		if err != nil {
			return nil, fmt.Errorf("dial winrm: %w", err)
		}
		return client, nil
	}
}

func Run(ctx context.Context, info provider.ConnectInfo, options ConnectorOptions) error {
	client, err := Dial(ctx, info, DialOptions{UseExternalAddr: options.UseExternalAddr})
	if err != nil {
		return err
	}
	defer client.Close()

	return client.Run(options.RunOptions)
}

// dialer is a retry-dialer. Typically, a connection timeout set by the OS will
// always take precedence over any caller provided timeout. This dialer
// continues to dial after an OS interruption with whatever time it has left.
type dialer struct {
	Timeout   time.Duration
	KeepAlive time.Duration
}

func (d *dialer) DialContext(ctx context.Context, network, address string) (conn net.Conn, err error) {
	ctx, cancel := context.WithTimeout(ctx, d.Timeout)
	defer cancel()

	base := net.Dialer{KeepAlive: d.KeepAlive}

	for ctx.Err() == nil {
		conn, err = base.DialContext(ctx, network, address)
		if err == nil {
			break
		}
	}

	return conn, err
}

func hostport(host string, defaultPort string) string {
	h, p, err := net.SplitHostPort(host)
	if err != nil {
		return net.JoinHostPort(host, defaultPort)
	}
	if p == "" {
		p = defaultPort
	}

	return net.JoinHostPort(h, p)
}
