package dummy

import (
	"crypto/rand"
	"encoding/hex"

	"gitlab.com/jobd/fleeting/fleeting/provider"
)

func (g *InstanceGroup) init() {
	if g.instances == nil {
		g.instances = make(map[string]*Instance)

		if g.MaxSize == 0 {
			g.MaxSize = 10000
		}
	}
}

func (g *InstanceGroup) List() []Instance {
	g.mu.Lock()
	defer g.mu.Unlock()

	g.init()

	var instances []Instance
	for _, inst := range g.instances {
		instances = append(instances, Instance{
			ID:    inst.ID,
			State: inst.State,
		})
	}

	return instances
}

func (g *InstanceGroup) AddInstance(state provider.State) string {
	g.mu.Lock()
	defer g.mu.Unlock()

	g.init()

	var b [8]byte
	rand.Read(b[:])

	id := hex.EncodeToString(b[:])
	g.instances[id] = &Instance{
		ID:    id,
		State: state,
	}

	return id
}

func (g *InstanceGroup) RemoveInstance(id string) {
	g.mu.Lock()
	defer g.mu.Unlock()

	delete(g.instances, id)
}

func (g *InstanceGroup) SetInstanceState(id string, state provider.State) {
	g.mu.Lock()
	defer g.mu.Unlock()

	inst, ok := g.instances[id]
	if !ok {
		return
	}

	inst.State = state
}
