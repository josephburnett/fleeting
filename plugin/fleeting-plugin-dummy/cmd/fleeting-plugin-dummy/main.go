package main

import (
	"gitlab.com/jobd/fleeting/fleeting/plugin"
	dummy "gitlab.com/jobd/fleeting/fleeting/plugin/fleeting-plugin-dummy"
)

func main() {
	plugin.Serve(&dummy.InstanceGroup{})
}
