package fleeting

import (
	"context"
	"errors"
	"fmt"
	"sync"
	"time"

	"github.com/hashicorp/go-hclog"
	"gitlab.com/jobd/fleeting/fleeting/provider"
)

type Provisioner struct {
	log   hclog.Logger
	group provider.InstanceGroup
	opts  options
	info  provider.ProviderInfo

	mu        sync.Mutex
	request   []request            // requests for new instances
	pending   []request            // inflight requests
	updated   []Instance           // instances updated in the last cycle
	instances map[string]*instance // current instances, reconciled with instance group

	closed   chan struct{}
	shutdown func()
}

var (
	ErrMaxSizeExceedsInstanceGroupMax = errors.New("max size option exceeds instance group's max size")
)

type request struct {
	requestedAt time.Time
}

func Init(ctx context.Context, log hclog.Logger, group provider.InstanceGroup, opts ...Option) (*Provisioner, error) {
	if log == nil {
		log = hclog.Default()
	}

	a := &Provisioner{
		log:       log,
		group:     group,
		closed:    make(chan struct{}),
		instances: make(map[string]*instance),
	}

	err := loadOptions(&a.opts, opts)
	if err != nil {
		return nil, fmt.Errorf("option: %w", err)
	}

	if err := validateConnectorConfig(&a.opts.settings.ConnectorConfig); err != nil {
		return nil, err
	}

	a.info, err = group.Init(ctx, log, a.opts.settings)
	if err != nil {
		return nil, fmt.Errorf("instance group init: %w", err)
	}

	if a.opts.maxSize > a.info.MaxSize {
		return nil, fmt.Errorf("%w: %d > %d", ErrMaxSizeExceedsInstanceGroupMax, a.opts.maxSize, a.info.MaxSize)
	} else if a.opts.maxSize > 0 {
		a.info.MaxSize = a.opts.maxSize
	}

	a.log = a.log.With("group", a.info.ID)

	// attempt to reconcile
	for i := 0; i < 5; i++ {
		err = a.reconcile(ctx, true)
		if err == nil {
			break
		}
		time.Sleep(a.opts.updateIntervalWhenExpecting)
	}
	if err != nil {
		return nil, fmt.Errorf("reconciling with instance group: %w", err)
	}
	if ctx.Err() != nil {
		return nil, ctx.Err()
	}

	ctx, cancel := context.WithCancel(context.Background())
	a.shutdown = cancel

	go func() {
		defer cancel()

		for {
			start := time.Now()

			a.removal(ctx)
			err := a.reconcile(ctx, false)
			// only provision if we're up-to-date
			if err == nil {
				a.provision(ctx)
			} else {
				log.Error("reconcile", "err", err)
			}

			if ctx.Err() != nil {
				return
			}

			time.Sleep(a.optimalUpdateInterval() - time.Since(start))
		}
	}()

	return a, nil
}

// Shutdown shuts down the provisioner.
//
// This function blocks until either all instances marked for deletion enter
// their delation state, the context is canceled or ShutdownDeletionRetries is
// exceeded.
func (a *Provisioner) Shutdown(ctx context.Context) {
	select {
	case <-a.closed:
		return
	default:
		close(a.closed)
	}

	a.shutdown()

	// request deletions
	for i := 0; i < a.opts.shutdownDeletionRetries && a.removal(ctx) > 0 && ctx.Err() == nil; i++ {
		time.Sleep(a.opts.shutdownDeletionInterval)
		a.reconcile(ctx, false)
	}
}

// Request requests new instances to be created. Multiple calls to Request will
// be coalesced and if the Instance Group supports it, will be performed in a
// single request.
//
// Any requests that exceed Instance Group capacity will be queued.
func (a *Provisioner) Request(n int) {
	select {
	case <-a.closed:
		return
	default:
	}

	req := make([]request, 0, n)
	for i := 0; i < n; i++ {
		req = append(req, request{requestedAt: time.Now()})
	}

	a.mu.Lock()
	defer a.mu.Unlock()

	a.request = append(a.request, req...)
}

// Instances returns active (creating, running, deleting) instances the
// provisioner is aware of.
//
// It does not return pending requests for a new instance or pending requests
// for deletions of instances. Use Capacity() to get inflight request data.
//
// The information returned can be out-of-date if an Instance Group update
// cannot or has not yet updated.
func (a *Provisioner) Instances() []Instance {
	a.mu.Lock()
	defer a.mu.Unlock()

	instances := make([]Instance, 0, len(a.instances))
	for _, inst := range a.instances {
		instances = append(instances, inst)
	}

	return instances
}

type Capacity struct {
	Creating  int
	Running   int
	Deleting  int
	Requested int
	Max       int
}

// Capacity returns the current count of instances, those requested and
// max capacity.
func (a *Provisioner) Capacity() Capacity {
	a.mu.Lock()
	defer a.mu.Unlock()

	c := Capacity{
		Requested: len(a.request) + len(a.pending),
		Max:       a.info.MaxSize,
	}

	for _, inst := range a.instances {
		if !inst.deletedAt.IsZero() {
			c.Deleting++
			continue
		}

		switch inst.state {
		case provider.StateCreating:
			c.Creating++
		case provider.StateRunning:
			c.Running++
		case provider.StateDeleting:
			c.Deleting++
		}
	}

	return c
}

func (a *Provisioner) reconcile(ctx context.Context, init bool) error {
	updateStart := time.Now()

	err := a.group.Update(ctx, func(id string, state provider.State) {
		a.mu.Lock()
		defer a.mu.Unlock()

		inst, ok := a.instances[id]
		if !ok {
			switch state {
			// never add newly added deleted/timeout instance
			case provider.StateDeleted, provider.StateTimeout:
				return
			}

			a.instances[id] = &instance{
				id:            id,
				provisioner:   a,
				state:         state,
				ready:         make(chan struct{}),
				provisionedAt: time.Now(),
				updatedAt:     time.Now(),
			}
			inst = a.instances[id]
			a.update(inst)

			switch {
			case init:
				inst.cause = CausePreexisted
			default:
				if len(a.pending) == 0 {
					inst.cause = CauseUnexpected
				} else {
					inst.cause = CauseRequested
				}
			}

			a.log.Info("instance discovery", "id", id, "state", state, "cause", inst.Cause())

			if len(a.pending) > 0 {
				inst.requestedAt = a.pending[0].requestedAt
				a.pending = a.pending[1:]
			}

			if state == provider.StateRunning {
				close(inst.ready)
			}
			return
		}

		if state != inst.state {
			a.log.Info("instance update", "id", id, "state", state)
			if state == provider.StateRunning {
				close(inst.ready)
			}
			a.update(inst)
		}
		inst.state = state
		inst.updatedAt = time.Now()
		inst.missedUpdates = 0
	})

	if err != nil {
		return err
	}

	a.prune(updateStart)
	a.notify()

	return nil
}

func (a *Provisioner) removal(ctx context.Context) int {
	var deletion []string

	// only instances that are "active" (in a.instances) are continued to be
	// scheduled for deletion. This prevents us from continuously trying to
	// remove an instance that has been pruned.
	a.mu.Lock()
	for _, inst := range a.instances {
		switch inst.state {
		case provider.StateDeleting, provider.StateDeleted, provider.StateTimeout:
			continue
		}

		// retry deletions if request appears to have had no affect.
		if inst.removing && time.Since(inst.deletedAt) > a.opts.deletionRetryInterval {
			deletion = append(deletion, inst.ID())
		}
	}
	a.mu.Unlock()

	if len(deletion) > 0 {
		succeeded, err := a.group.Decrease(ctx, deletion)
		if err != nil {
			a.log.Error("decreasing instances", "err", err)
		}

		a.mu.Lock()
		for _, id := range succeeded {
			a.instances[id].deletedAt = time.Now()
		}
		a.mu.Unlock()
	}

	return len(deletion)
}

func (a *Provisioner) provision(ctx context.Context) {
	var demand int

	a.mu.Lock()
	{
		demand = len(a.request)
		capacity := a.info.MaxSize - len(a.pending) - len(a.instances)
		if demand > capacity {
			demand = capacity
		}
	}
	a.mu.Unlock()

	if demand <= 0 {
		return
	}

	added, err := a.group.Increase(ctx, demand)
	if err != nil {
		a.log.Error("increase instances", "num_requested", demand, "num_successful", added, "err", err)
	}
	if added > demand {
		a.log.Error("provider increased instances beyond those requested", "num_requested", demand, "num_successful", added)
	}
	if added <= 0 {
		return
	}

	a.mu.Lock()
	defer a.mu.Unlock()

	a.pending = append(a.pending, a.request[:added]...)
	a.request = a.request[added:]
}

func (a *Provisioner) prune(updateStart time.Time) {
	a.mu.Lock()
	defer a.mu.Unlock()

	for id := range a.instances {
		inst := a.instances[id]

		switch inst.state {
		// if a provider sets an instance to deleted
		// or timeout, it's removed from the list
		case provider.StateDeleted, provider.StateTimeout:
			delete(a.instances, id)
			continue
		}

		if inst.updatedAt.Before(updateStart) {
			if inst.state == provider.StateDeleting {
				inst.state = provider.StateDeleted
				inst.updatedAt = time.Now()
				delete(a.instances, id)

				a.update(inst)
				continue
			}

			inst.missedUpdates++
			if inst.missedUpdates > 5 {
				a.log.Info("instance pruned", "id", id, "missed_updates", inst.missedUpdates, "last_updated_at", inst.updatedAt)
				inst.state = provider.StateTimeout
				delete(a.instances, id)

				a.update(inst)
			}
		}
	}
}

func (a *Provisioner) update(inst Instance) {
	if a.opts.subscriber == nil {
		return
	}

	a.updated = append(a.updated, inst)
}

func (a *Provisioner) notify() {
	if a.opts.subscriber == nil {
		return
	}

	a.mu.Lock()
	updated := make([]Instance, len(a.updated))
	copy(updated, a.updated)
	a.updated = a.updated[:0]
	a.mu.Unlock()

	go a.opts.subscriber(updated)
}

func (a *Provisioner) optimalUpdateInterval() time.Duration {
	a.mu.Lock()
	defer a.mu.Unlock()

	if len(a.pending) > 0 {
		return a.opts.updateIntervalWhenExpecting
	}

	for _, inst := range a.instances {
		if inst.state != provider.StateRunning {
			return a.opts.updateIntervalWhenExpecting
		}
	}

	return a.opts.updateInterval
}
